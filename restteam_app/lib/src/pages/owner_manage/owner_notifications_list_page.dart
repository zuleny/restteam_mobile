import 'package:flutter/material.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/preferences/http.dart';


class OwnerNotification extends StatefulWidget {
  static final String routeName = 'owner_notification_list';
  @override
  _OwnerNotificationState createState() => _OwnerNotificationState();
}

class _OwnerNotificationState extends State<OwnerNotification> {
  final prefs = new Preferences();
  Http http = new Http();
  List ownerNotificationList = [];
  bool state = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = Provider.of(context);
    print(ownerNotificationList);
    if (ownerNotificationList.length == 0 && !state) {
      _getOwnerNotificationList(bloc.password);
      state = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Mis Notificaciones'),
          backgroundColor: _getColor(prefs.color),
        ),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  _getList(BuildContext context) {
    if (ownerNotificationList.length == 0 && state) {
      return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Sin Notificaciones Nuevas!.",
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: Colors.deepPurple,
                      fontStyle: FontStyle.italic),
                ),
              )
            ],
          ));
    }
    return ListView.builder(
      itemCount: ownerNotificationList == null ? 0 : ownerNotificationList.length,
      itemBuilder: (context, index) =>
          _createItem(ownerNotificationList[index], context),
    );
  }

  _createItem(var list, BuildContext context) {
    return Card(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListTile(
            leading: Text(
              "${list['request_no']}",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 20,
                color: Colors.green,
              )
            ),
            title: Text(
              "Cod. Producto: ${list['cod_product']}",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 16,
                color: Colors.deepPurple,
              ),
            ),
            subtitle: Text(
              "${list['description_product']}\nFecha: ${list['service_date']}",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 15,
                color: Colors.grey,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ));
  }

  _getOwnerNotificationList(String ci) async {
    Map data = await http.get("/owner_management/owner_manage/notifications/$ci");
    var val = data['notifications'];
    print(val);
    setState(() {
      ownerNotificationList = val;
    });
  }

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.deepPurple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}