import 'package:flutter/material.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/preferences/http.dart';


class OwnerActivity extends StatefulWidget {
  static final String routeName = 'owner_activity_list';
  @override
  _OwnerActivityState createState() => _OwnerActivityState();
}

class _OwnerActivityState extends State<OwnerActivity> {
  final prefs = new Preferences();
  Http http = new Http();
  List ownerActivityList = [];
  bool state = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = Provider.of(context);
    print(ownerActivityList);
    if (ownerActivityList.length == 0 && !state) {
      _getOwnerActivityList(bloc.email);
      state = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Mis Actividades'),
          backgroundColor: _getColor(prefs.color),
        ),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  _getList(BuildContext context) {
    if (ownerActivityList.length == 0 && state) {
      return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Sin Actividades!.",
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: Colors.deepPurple,
                      fontStyle: FontStyle.italic),
                ),
              )
            ],
          ));
    }
    return ListView.builder(
      itemCount: ownerActivityList == null ? 0 : ownerActivityList.length,
      itemBuilder: (context, index) =>
          _createItem(ownerActivityList[index], context),
    );
  }

  _createItem(var list, BuildContext context) {
    return Card(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListTile(
            leading: Icon(Icons.timeline, color: Colors.green,),
            title: Text(
              "Fecha: ${list['date']}",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 16,
                color: Colors.deepPurple,
              ),
            ),
            subtitle: Text(
              "Actividad: ${list['activity']}",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 15,
                color: Colors.grey,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ));
  }

  _getOwnerActivityList(String email) async {
    print("Email: "+email);
    Map data = await http.get("/owner_management/backlog_manage/$email");
    var val = data['bitacoraData'];
    print(val);
    setState(() {
      ownerActivityList = val;
    });
    print(val);
  }

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.deepPurple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}