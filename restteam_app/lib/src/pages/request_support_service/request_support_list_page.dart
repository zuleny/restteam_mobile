import 'package:flutter/material.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_page.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/preferences/http.dart';

class RequestSupportList extends StatefulWidget {
  static final String routeName = 'request_support_service_list';
  @override
  _RequestSupportListState createState() => _RequestSupportListState();
}

class _RequestSupportListState extends State<RequestSupportList> {
  final prefs = new Preferences();
  Http http = new Http();
  List requestSupportList = [];
  bool state = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = Provider.of(context);
    int ci = int.parse(bloc.password);
    print(requestSupportList);
    if (requestSupportList.length == 0 && !state) {
      _getSupportServiceList(ci);
      state = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('All Request Support'),
          backgroundColor: _getColor(prefs.color),
        ),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  _getList(BuildContext context) {
    if (requestSupportList.length == 0 && state) {
      return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)),
        child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconButton(
            padding: EdgeInsets.all(20.0),
            tooltip: 'Request Support Now!',
            //splashColor: Colors.blueAccent,
            onPressed: () {
              Navigator.pushNamed(context, RequestSupport.routeName);
            },
            icon: Icon(
              Icons.add_circle,
              color: Colors.green,
              size: 40,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "No request yet!.",
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 16,
                  color: Colors.deepPurple,
                  fontStyle: FontStyle.italic),
            ),
          )
        ],
      ));
    }
    return ListView.builder(
      itemCount: requestSupportList == null ? 0 : requestSupportList.length,
      itemBuilder: (context, index) =>
          _createItem(requestSupportList[index], context),
    );
  }

  _createItem(var request, BuildContext context) {
    var policy = request['cod_policy'];
    var company = request['get_company_name'];
    if (company == null) {
      company = "none";
    }
    if (policy == "") {
      policy = "none";
    }
    return Card(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListTile(
            leading: _getIcon(request['state_request']),
            title: Text(
              "Policy: $policy",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 16,
                color: Colors.deepPurple,
              ),
            ),
            subtitle: Text(
              "Type: ${request['description_type']}\nCompany: $company\nDate: ${request['date_request']}",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 15,
                color: Colors.grey,
                fontStyle: FontStyle.italic,
              ),
            ),
            onTap: () => _moreInformation(context, request),
          ),
        ));
  }

  Widget _getIcon(int state) {
    if (state == 1) {
      return Icon(Icons.check_circle, color: Colors.green);
    }
    if (state == 2) {
      return Icon(
        Icons.watch_later,
        color: Colors.orangeAccent,
      );
    }
    return Icon(
      Icons.cancel,
      color: Colors.teal,
    );
  }

  _moreInformation(BuildContext context, var request) {
    var company = request['get_company_name'];
    var policy = request['cod_policy'];
    if (company == null) {
      company = "none";
    }

    if (policy == "") {
      policy = "none";
    }
    Widget icon = _getIcon(request['state_request']);

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: ListTile(
            leading: _getIcon(request['state_request']),
            title: Text(
              "Request N° ${request['request_no']}",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Colors.deepPurple,
                  fontStyle: FontStyle.italic),
            ),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Divider(color: Colors.deepPurple,),
              _getRowData("Date", request['date_request']),
              _getRowData("Policy", policy),
              _getRowData("Company", company),
              Divider(color: Colors.deepPurple,),
              _getRowData("Type", request['description_type']),
              Row(
                children: <Widget>[
                  Text(
                    "Product: ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.green,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(
                "${request['description_product']}",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic),
              ),
              Row(
                children: <Widget>[
                  Text(
                    "Problem: ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.green,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(
                "${request['description']}",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic),
              ),
              Divider(color: Colors.deepPurple,),
            ],
          ),
          scrollable: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        );
      },
    );
  }

  _getRowData(var name, var value) {
    return Row(
      children: <Widget>[
        Text(
          "$name: ",
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic),
        ),
        Text(
          "$value",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              color: Colors.grey,
              fontStyle: FontStyle.italic),
        ),
      ],
    );
  }

  _getSupportServiceList(int ci) async {
    Map data =
        await http.get("/owner_management/reques_support_manage/list/$ci");
    var val = data['requestList'];
    print(val);
    setState(() {
      requestSupportList = val;
    });
    print(val);
  }

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.deepPurple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}
