import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:restteam_app/src/pages/product_manage/product_list_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_list_page.dart';

import 'package:restteam_app/src/pages/request_support_service/request_support_policy_page.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import '../home_page.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';

class RequestSupport extends StatefulWidget {
  static final String routeName = 'request_support';
  String futureString = '';

  @override
  _RequestSupportState createState() => _RequestSupportState();
}

class _RequestSupportState extends State<RequestSupport> {
  final prefs = new Preferences();
  String futureString = '';
  List policy;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text("Tipo de Solicitud de Servicio Técnico"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                  prefs.getColor(prefs.color),
                  prefs.getSecondaryColor(prefs.color)
                ])),
          )),
      drawer: MenuWidget(),
      body: Center(
        child: _buttonsBordered(context),
      ),
    );
  }

  Widget _buttonsBordered(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 100.0),
      children: <Widget>[
        Card(
          child: _buttonQR(context),
        ),
        Card(child: _buttonWithoutGuarantee(context)),
        Card(
          child: ListTile(
            title: Text(
              "All Request",
              style: TextStyle(
                  color: Colors.deepPurple,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
            subtitle: Text(
              "List Of All request",
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
            leading: Icon(
              Icons.format_list_numbered,
              color: Colors.green,
            ),
            onTap: () {
              Navigator.pushNamed(context, RequestSupportList.routeName);
            },
          ),
        )
      ],
    );
  }

  Widget _buttonWithoutGuarantee(BuildContext context) {
    return ListTile(
      leading: Icon(
        Icons.assignment,
        color: Colors.green,
      ),
      title: Text('Solicitud de servicio técnico sin póliza de garantía',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.italic)),
      subtitle: Text('Si tu producto no tiene garantía, elije esta opción :)'),
      onTap: () {
        Navigator.pushNamed(context, ProductList.routeName);
      },
    );
  }

  Widget _buttonQR(BuildContext context) {
    return ListTile(
        leading: Icon(
          Icons.center_focus_strong,
          color: Colors.green,
        ),
        title: Text(
          'Scanner QR',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.italic),
        ),
        subtitle: Text('Scann QR Of Your Product!'),
        onTap: () {
          _scanQR(context);
        });
  }

  _scanQR(BuildContext context) async {
    try {
      futureString = await scanner.scan();
    } catch (e) {
      futureString = e.toString();
    }
    print("futureString: $futureString");
    if (futureString != null) {
      print("There is information");
      if (validateData(futureString)) {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => RequestSupportPolicy(futureString)));
      }
    } else {
      print("There isnt information");
      Navigator.pushReplacementNamed(context, HomePage.routeName);
    }
  }

  bool validateData(String data) {
    try {
      int index = data.indexOf('@');
      int lastIndex = data.lastIndexOf('@');
      String codPolicy = data.substring(0, index);
      String ciOwner = data.substring(index + 1, lastIndex);
      String codOwner = data.substring(lastIndex + 1);
      print('$codPolicy, $ciOwner, $codOwner');
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.purple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}
