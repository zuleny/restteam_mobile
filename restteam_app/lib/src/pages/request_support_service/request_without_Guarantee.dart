import 'package:flutter/material.dart';
import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/home_page.dart';
import 'package:restteam_app/src/preferences/http.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:toast/toast.dart';

class RequestWithoutPolicy extends StatefulWidget {
  static final String routeName = 'request_without_policy';
  int codProductOwner;

  RequestWithoutPolicy(this.codProductOwner);
  @override
  _RequestWithoutPolicyState createState() => _RequestWithoutPolicyState();
}

class _RequestWithoutPolicyState extends State<RequestWithoutPolicy> {
  final _formKey = GlobalKey<FormState>();
  final prefs = new Preferences();
  Http http = new Http();
  int _codProductOwner;
  List<DropdownMenuItem<String>> _dropMenuTypeSupport = [];
  List _dataRequestProductOwner = [];
  String _typeSupportSelected;
  String _descriptionProblem = "";
  @override
  void initState() {
    super.initState();
    _codProductOwner = this.widget.codProductOwner;
    _getDataAndListOptions(_codProductOwner);
    setState(() {});
  }

  _getDataAndListOptions(codProductOwner) async {
    try {
      Map response = await http.get(
          "/owner_management/request_support_manage/without_guarantee/$_codProductOwner");
      _dataRequestProductOwner = response['data'];
      _dropMenuTypeSupport =
          await _getListTypeSupport(response['listTypeSupport']);
      setState(() {});
    } catch (e) {
      print("Error in getDataAndListOption $e");
    }
  }

  _getListTypeSupport(List listTypeService) {
    List<DropdownMenuItem<String>> list = List();
    _typeSupportSelected = listTypeService[0]["description_type"];
    for (int i = 0; i < listTypeService.length; i++) {
      list.add(DropdownMenuItem(
          value: listTypeService[i]["description_type"],
          child: Text("${listTypeService[i]["description_type"]}")));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text("Registrar Solicitud de Servicio Técnico"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                  prefs.getColor(prefs.color),
                  prefs.getSecondaryColor(prefs.color)
                ])),
          )),
      drawer: MenuWidget(),
      body: _formRequestData(context),
    );
  }

  Widget _formRequestData(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  _descriptionRequest(),
                  _getListOptionsTypeSupport(),
                  _getBoxDescription(),
                  _buttonSendRequest(context)
                ],
              ),
            )),
      ),
    );
  }

  Widget _descriptionRequest() {
    String messageBox = "Loading..";
    if (_dataRequestProductOwner.length > 0) {
      messageBox =
          "Código: ${_dataRequestProductOwner[0]["code"]}\nNombre Propietario: ${_dataRequestProductOwner[0]["name"]}\nModelo: ${_dataRequestProductOwner[0]["model"]}\nMarca: ${_dataRequestProductOwner[0]["brand"]}\nDescripción: ${_dataRequestProductOwner[0]["description_product"]}\nCategoría: ${_dataRequestProductOwner[0]["description_category"]}";
    }
    return Container(
      decoration: BoxDecoration(
          color: prefs.getColor(prefs.color),
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: prefs.getSecondaryColor(prefs.color),
                blurRadius: 6.0,
                offset: Offset(0.0, 5.0))
          ]),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(messageBox, style: TextStyle(color: Colors.white)),
      ),
    );
  }

  Widget _getListOptionsTypeSupport() {
    try {
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.transfer_within_a_station),
            Text('Tipo de Servicio: '),
            DropdownButton(
              value: _typeSupportSelected,
              items: _dropMenuTypeSupport,
              onChanged: (value) {
                setState(() {
                  _typeSupportSelected = value;
                });
              },
            )
          ]);
    } catch (e) {
      print("Error get list Type Support Service in Request without guarantee");
      return Center(
        child: Text("Error", style: TextStyle(color: Colors.red)),
      );
    }
  }

  Widget _getBoxDescription() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            counter: Text("letters: ${_descriptionProblem.length}"),
            labelText: "Descripción del Problema",
            helperText:
                "Debes explicar el problema detalladamente que presenta tu producto",
            icon: Icon(Icons.textsms)),
        validator: (value) {
          if (value.length > 10) {
            return null;
          } else {
            return "Sea mas explícito";
          }
        },
        onChanged: (value) {
          setState(() {
            _descriptionProblem = value;
          });
        },
      ),
    );
  }

  Widget _buttonSendRequest(BuildContext context) {
    return RaisedButton.icon(
      icon: Icon(Icons.send),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text("Enviar la Solicitu de Servicio Técnico"),
      onPressed: () => showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text("Estas seguro Amigo?"),
            content: Text(
                "Estas seguro con la descripcion del problema y el tipo de servicio?"),
            actions: <Widget>[
              RaisedButton(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.cancel, color: Colors.white),
                    Text("No")
                  ],
                ),
                color: Colors.red,
                onPressed: () => Navigator.of(context).pop(),
              ),
              RaisedButton(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.check_circle, color: Colors.white),
                    Text("Si")
                  ],
                ),
                color: Colors.green,
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    int result = await _sendRequest();
                    if (result > 0) {
                      Future.delayed(Duration(seconds: 1), () {
                        _showToast(
                            "Solicitud No. $result Enviado Correctamente.",
                            context,
                            duration: Toast.LENGTH_LONG,
                            gravity: Toast.BOTTOM);
                      });
                      Navigator.pushNamed(context, HomePage.routeName);
                    } else {
                      Navigator.of(context).pop();
                    }
                  }
                },
              )
            ],
          );
        },
      ),
    );
  }

  Future<int> _sendRequest() async {
    try {
      print(
          "data: description $_descriptionProblem type support: $_typeSupportSelected codProductOwner: $_codProductOwner");
      var data = {
        "description_problem": _descriptionProblem,
        "type_support_service": _typeSupportSelected,
        "cod_product_owner": _codProductOwner
      };
      Map result = await http.post(
          "/owner_management/request_support_manage/register_without_guarantee",
          data);
      print(result);
      return result['result'];
    } catch (e) {
      print("Error to send request: $e");
      return -1;
    }
  }

  void _showToast(String msg, BuildContext context,
      {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}
