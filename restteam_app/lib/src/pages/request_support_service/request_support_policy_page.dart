import 'package:flutter/material.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_page.dart';

import 'package:restteam_app/src/preferences/http.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';

class RequestSupportPolicy extends StatefulWidget {
  static final String routeName = 'request_support_policy';
  String data;
  RequestSupportPolicy(data_i) {
    this.data = data_i;
  }

  @override
  _RequestSupportPolicyState createState() => _RequestSupportPolicyState();
}

class _RequestSupportPolicyState extends State<RequestSupportPolicy> {
  final prefs = new Preferences();
  Http http = new Http();
  List _listOptions = [];
  String description = '';
  String optionSelected;
  String codPolicy;
  String ciOwner;
  String codOwner;

  @override
  void initState() {
    // TODO: implement initState
    setData(this.widget.data);
    super.initState();
    getListTypeSupport();
  }

  setData(String data) {
    int index = data.indexOf('@');
    int lastIndex = data.lastIndexOf('@');
    codPolicy = data.substring(0, index);
    ciOwner = data.substring(index + 1, lastIndex);
    codOwner = data.substring(lastIndex + 1);
  }

/*
 * 
 */
  getListTypeSupport() async {
    try {
      Map data = await http
          .get("/owner_management/reques_support_manage/type_support_list");
      setState(() {
        _listOptions = data['type_support'];
      });
      optionSelected = _listOptions[0]['description_type'];
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text("Solicitud Soporte Técnico"),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                    prefs.getColor(prefs.color),
                    prefs.getSecondaryColor(prefs.color)
                  ])),
            )),
        drawer: MenuWidget(),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          children: <Widget>[
            _cardInfo(),
            Divider(
              color: Colors.deepPurple,
            ),
            _dropDown(),
            _createInput(),
            Divider(
              color: Colors.deepPurple,
            ),
            _button(),
          ],
        ));
  }

  Widget _cardInfo() {
    return Card(
      margin: EdgeInsets.all(8.0),
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _getRowData("Policy", codPolicy),
            _getRowData("CI", ciOwner),
            _getRowData("Code", codOwner)
          ],
        ),
      ),
    );
  }

  _getRowData(var name, var value) {
    return Row(
      children: <Widget>[
        Text(
          "$name: ",
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic),
        ),
        Text(
          "$value",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              color: Colors.grey,
              fontStyle: FontStyle.italic),
        ),
      ],
    );
  }

  Widget _dropDown() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.build,
            color: Colors.green,
          ),
          SizedBox(width: 40.0),
          DropdownButton(
              value: optionSelected,
              items: getListOptions(),
              onChanged: (opt) {
                setState(() {
                  optionSelected = opt;
                });
              })
        ],
      ),
    );
  }

  List<DropdownMenuItem<String>> getListOptions() {
    List<DropdownMenuItem<String>> list = new List();
    for (int j = 0; j < _listOptions.length; j++) {
      String ele = "${_listOptions[j]['description_type']}";
      list.add(DropdownMenuItem(
        child: Text("$ele"),
        value: ele,
      ));
    }
    return list;
  }

  Widget _createInput() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textCapitalization: TextCapitalization.none,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            counter: Text("letras: ${description.length}"),
            hintText: "problems with conection internet",
            labelText: "Description",
            helperText: "Description Here",
            icon: Icon(Icons.format_list_numbered)),
        onChanged: (val) {
          setState(() {
            description = val;
          });
        },
      ),
    );
  }

  Widget _button() {
    return RaisedButton.icon(
      icon: Icon(Icons.send),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text("Enviar la Solicitu de Servicio Técnico"),
      onPressed: () => _registerRequest(context),
    );
  }

  _registerRequest(BuildContext context) async {
    try {
      var now = new DateTime.now();
      var values = {
        'description': description,
        'cod_policy': codPolicy,
        'date_request': now.toString(),
        'description_type': optionSelected,
        'code_owner': codOwner,
        'ci_owner': ciOwner
      };
      Map data =
          await http.post("/owner_management/reques_support_manage", values);
      var val = data['idRequest'];
      String message = "";
      if (val > 0) {
        message =
            "Technical support request sent successfully! \nRequest number: $val.\nOur team will respond to your request as soon as possible, please stay tuned to your email inbox";
        _showDialog(context, message);
      } else {
        message = "Request Support Service Fail!  Try later!";
        _showDialog(context, message);
      }
    } catch (e) {
      print(e);
    }
  }

  _showDialog(BuildContext context, String message) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: Text("Request Support Service"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("$message"),
            ],
          ),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  //Navigator.pop(context);
                  Navigator.pushReplacementNamed(
                      context, RequestSupport.routeName);
                },
                child: Text("Ok"))
          ],
        );
      },
    );
  }

  /**
   * Method to get Color
   */

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.purple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}
