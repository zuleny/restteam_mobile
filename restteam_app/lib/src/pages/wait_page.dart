import 'package:flutter/material.dart';


class Wait extends StatelessWidget {
  static final String routeName = 'wait';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Wait...'),
            Divider(),
          ],
        ));
  }

}