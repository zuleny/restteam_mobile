import 'package:restteam_app/src/pages/owner_manage/owner_notifications_list_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_page.dart';
import 'package:restteam_app/src/preferences/http.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:toast/toast.dart';

class HomePage extends StatefulWidget {
  static final String routeName = 'home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final prefs = new Preferences();
  Http http = new Http();
  bool selected = false;
  bool state = false;
  List _horses = [];
  List _notifications = [];
  bool stateNotifications = false;

  _getProductList(int ci) async {
    Map data = await http.get("/owner_management/product_manage/owner");
    var val = data['listProducts'];
    setState(() {
      _horses = val;
    });
  }

  _getNotificatonsList(int ci) async {
    Map data = await http.get("/owner_management/owner_manage/notifications/$ci");
    print(data);
    var val = data['notifications'];
    setState(() {
      _notifications = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    int ci = int.parse(bloc.password);
    if (_horses.length == 0 && !state) {
      _getProductList(ci);
      state = true;
    }

    if (_notifications.length == 0 && !stateNotifications){
      _getNotificatonsList(ci);
      stateNotifications = true;
      print(_notifications);
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("RestTeam"),
        actions: <Widget>[
          PopupMenuButton(
            color: Colors.white70,
            itemBuilder: (BuildContext context){
             return _getListNotifications(context);
            },
            icon: Icon(Icons.notifications, color: Colors.white,),
          )
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                prefs.getColor(prefs.color),
                prefs.getSecondaryColor(prefs.color)
              ])),
        ),
      ),
      drawer: MenuWidget(),
      body: _getHomePageBody(context,ci),
      floatingActionButton: _createButton(context),
    );
  }

  Widget _createButton(BuildContext context) {
    return FloatingActionButton(
        child: Icon(Icons.add, size: 35),
        tooltip: "Solicite Soporte",
        backgroundColor: prefs.getColor(prefs.color),
        onPressed: () => Navigator.pushNamed(context, RequestSupport.routeName));
  }

  Widget _getHomePageBody(BuildContext context, int ci) {
    if (_horses.length > 0 && state) {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: CarouselSlider(
                height: 520.0,
                initialPage: 0,
                enlargeCenterPage: false,
                autoPlay: true,
                autoPlayAnimationDuration: Duration(seconds: 3),
                enableInfiniteScroll: true,
                items: _horses.map((var e) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Builder(
                      builder: (context) {
                        return Container(
                          
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.bottomLeft, // 10% of the width, so there are ten blinds.
                                  end: Alignment.topRight, // 10% of the width, so there are ten blinds.
                                  colors: prefs.getBackgroundColor()
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.black38,
                                    blurRadius: 6.0,
                                    offset: Offset(2.0, 5.0))
                              ]),
                        
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                child: Image(image: NetworkImage("${http.ip}/photo/${e["product_image"]}")),
                                height: 300,
                                
                              ),
                              
                
                              Column(
                                  children: <Widget>[
                                    
                                    ListTile(
                                      contentPadding: EdgeInsets.all(5),
                                      title: Text(
                                        "Marca: ${e["brand"]}, Modelo: ${e["model"]}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.0,
                                          color: Colors.deepPurple

                                        )
                                      ),
                                      subtitle: Text(
                                        e["description_product"],
                                        style: TextStyle(fontSize: 12.0),
                                      ),
                                    ),

                                    RaisedButton.icon(
                                        icon: Icon(Icons.check),
                                        textColor: Colors.white,
                                        color: Colors.green,
                                        splashColor: Colors.grey,
                                        shape: StadiumBorder(),
                                        label: Text("Registrar"),
                                        onPressed: () => registerProductOwner(context,e["cod_product"],ci)
                                    )                                     

                                  ],
                                  
                                )

                            ],
                          ) ,
                        );
                      },
                    ),
                  );
                }).toList(),
              ),
            )
          ],
        ),
      );
    } else {
      return GestureDetector(
        onTap: () {
          setState(() {
            selected = !selected;
          });
        },
        child: Center(
          child: AnimatedContainer(
              width: selected ? 200.0 : 100.0,
              height: selected ? 100.0 : 200.0,
              color: selected ? prefs.getColor(prefs.color) : Colors.red,
              alignment:
                  selected ? Alignment.center : AlignmentDirectional.topCenter,
              duration: Duration(seconds: 3),
              curve: Curves.fastOutSlowIn,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.computer),
                    Text(
                      "No tienes ningun producto registrado",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
        ),
      );
    }
  }
  registerProductOwner(BuildContext context, int codProduct, int ciOwner) async {
    var data = {"cod_product": codProduct, "ci_owner": ciOwner};
    Map responseRegister = await http.post(
        "/owner_management/product_manage/owner/register_product", data);
    if (responseRegister['result'] > 0) {
      print("registred: ${responseRegister["result"]}");
      int codProductOwner = responseRegister['result'];
      _showToast("Producto $codProductOwner Registrado correctamente", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    } else {
      _showToast("Producto NO Registrado, Intente Nuevamente", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void _showToast(String msg, BuildContext context,
      {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
  
  List<PopupMenuEntry<String>> _getListNotifications(BuildContext context){
    List <PopupMenuEntry<String>> list = new List();
      _notifications.forEach((element) {
        list.add(
          new PopupMenuItem(
            child: Text(
              "Solicitud N° ${element['request_no']} HA CONCLUIDO,con cod. producto ${element['cod_product']}",
              style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 14,fontStyle: FontStyle.italic,),
            ),
            value: "${element['request_no']}",
          )
        );
      }
    );
    list.add(
      new PopupMenuItem(
        child: FlatButton(
          onPressed:() => Navigator.pushNamed(context, OwnerNotification.routeName),
          child: Text(
            "Ver Todos",
            style: TextStyle(color: Colors.green, fontWeight: FontWeight.w800, fontSize: 14)
          ),
        )
      )
    );
    return list;
  }
  
}
