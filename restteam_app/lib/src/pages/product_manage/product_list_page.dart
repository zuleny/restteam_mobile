import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/guarantee_policy_manage/guarantee_policy_page.dart';
import 'package:restteam_app/src/pages/product_manage/product_register_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_policy_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_without_Guarantee.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/preferences/http.dart';

class ProductList extends StatefulWidget {
  static final String routeName = 'product_list';
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final prefs = new Preferences();
  Http http = new Http();
  List productList = [];
  bool state = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = Provider.of(context);
    int ci = int.parse(bloc.password);
    print(productList);
    if (productList.length == 0 && !state) {
      _getProductList(ci);
      state = true;
    }

    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text("Lista de tus Productos"),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                    prefs.getColor(prefs.color),
                    prefs.getSecondaryColor(prefs.color)
                  ])),
            )),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  _getList(BuildContext context) {
    if (productList.length == 0 && state) {
      return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                padding: EdgeInsets.all(20.0),
                tooltip: "Register your Product Now!",
                onPressed: () {
                  Navigator.pushNamed(context, ProductPage.routeName);
                },
                icon: Icon(
                  Icons.add_circle,
                  color: Colors.green,
                  size: 40,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "You haven't product",
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: Colors.deepPurple,
                      fontStyle: FontStyle.italic),
                ),
              )
            ],
          ));
    }
    return ListView.builder(
      itemCount: productList == null ? 0 : productList.length,
      itemBuilder: (context, index) =>
          _createItem(productList[index], context, index),
    );
  }

  _createItem(var list, BuildContext context, int index) {
    return Slidable(
      key: ValueKey(index),
      actionPane: SlidableDrawerActionPane(),
      child: Card(
          key: UniqueKey(),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListTile(
              leading:
                  Icon(Icons.chevron_left, color: prefs.getColor(prefs.color)),
              title: Text(
                "Code: ${list['code']}",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 16,
                  color: Colors.deepPurple,
                ),
              ),
              subtitle: Text(
                "Brand: ${list['brand']}\nModel: ${list['model']}",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 15,
                  color: Colors.grey,
                  fontStyle: FontStyle.italic,
                ),
              ),
              trailing: Icon(
                Icons.chevron_right,
                color: prefs.getColor(prefs.color),
              ),
              onTap: () => _moreInformation(context, list),
            ),
          )),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: "Solicitar Servicio",
          color: Colors.blue,
          icon: Icons.chrome_reader_mode,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RequestWithoutPolicy(list['code'])));
          },
        )
      ],
      actions: <Widget>[
        IconSlideAction(
          caption: "Registrar Póliza",
          color: Colors.indigo,
          icon: Icons.art_track,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => GuaranteePolicyPage(list['code'])));
          },
        )
      ],
    );
  }

  _moreInformation(BuildContext context, var list) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: ListTile(
            leading: Image(
              image: NetworkImage(
                  "${http.ip}/photo_product/${list['product_image']}"),
            ),
            title: Text(
              "Code Owner ${list['code']}",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Colors.deepPurple,
                  fontStyle: FontStyle.italic),
            ),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Category", list['description_category']),
              Divider(
                color: Colors.deepPurple,
              ),
              Row(
                children: <Widget>[
                  Text(
                    "Product: ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.green,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(
                "${list['description_product']}",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic),
              ),
              _getRowData("Brand", list['brand']),
              _getRowData("Model", list['model']),
              Divider(
                color: Colors.deepPurple,
              ),
            ],
          ),
          scrollable: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        );
      },
    );
  }

  _getRowData(var name, var value) {
    return Row(
      children: <Widget>[
        Text(
          "$name: ",
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic),
        ),
        Text(
          "$value",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              color: Colors.grey,
              fontStyle: FontStyle.italic),
        ),
      ],
    );
  }

  _getProductList(int ci) async {
    Map data = await http.get("/owner_management/product_manage/list/$ci");
    var val = data['productList'];
    print(val);
    setState(() {
      productList = val;
    });
    print(val);
  }
}
