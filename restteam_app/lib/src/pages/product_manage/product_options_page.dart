import 'package:flutter/material.dart';
import 'package:restteam_app/src/pages/guarantee_policy_manage/guarantee_policy_page.dart';
import 'package:restteam_app/src/pages/home_page.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';

class ProductOwnerOptions extends StatefulWidget {
  static final String routeName = 'product_owner_options';
  final int codProductOwner;

  ProductOwnerOptions(this.codProductOwner);

  @override
  _ProductOwnerOptionsState createState() => _ProductOwnerOptionsState();
}

class _ProductOwnerOptionsState extends State<ProductOwnerOptions> {
  final prefs = new Preferences();
  int _codProductOwnerSelected;
  @override
  void initState() {
    setState(() {
      _codProductOwnerSelected = this.widget.codProductOwner;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("¿Tiene Póliza de Garantía?"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                prefs.getColor(prefs.color),
                prefs.getSecondaryColor(prefs.color)
              ])),
        ),
      ),
      drawer: MenuWidget(),
      body: Center(child: _buttonsOptions(context)),
    );
  }

  Widget _buttonsOptions(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 150.0),
      children: <Widget>[
        Center(
          child: Column(
            children: <Widget>[
              _descriptionRegister(),
              _buttonPolicyGuarantee(context),
              _buttonConclusionProcess(context)
            ],
          ),
        )
      ],
    );
  }

  Widget _buttonPolicyGuarantee(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Icons.insert_drive_file, color: Colors.green),
        title: Text("Register your Guarantee Policy",
            style: TextStyle(
                color: Colors.deepPurple,
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic)),
        subtitle: Text("You can receive maintenance services"),
        onTap: () async {
          await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  GuaranteePolicyPage(_codProductOwnerSelected)));
        },
      ),
    );
  }

  Widget _buttonConclusionProcess(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Icons.apps, color: Colors.green),
        title: Text("I don't have a guarantee policy",
            style: TextStyle(
                color: Colors.deepPurple,
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic)),
        subtitle: Text("Close product registration process."),
        onTap: () {
          Navigator.pushNamed(context, HomePage.routeName);
        },
      ),
    );
  }

  Widget _descriptionRegister() {
    return Container(
      decoration: BoxDecoration(
          color: prefs.getColor(prefs.color),
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: prefs.getColor(prefs.color),
                blurRadius: 6.0,
                offset: Offset(0.0, 5.0))
          ]),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text("Register Code ${_codProductOwnerSelected}",
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w900,
                fontSize: 10.0)),
      ),
    );
  }
}
