import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/product_manage/product_options_page.dart';
import 'package:restteam_app/src/preferences/http.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';

class ProductPage extends StatefulWidget {
  static final String routeName = 'product_page';
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final prefs = new Preferences();
  Http http = new Http();
  Map listProducts;
  List listProductsSelect = [];

  Future<Null> getRefresh() async {
    await Future.delayed(Duration(seconds: 3));
  }

  getListProducts() async {
    try {
      listProducts = await http.get("/owner_management/product_manage/owner");
      setState(() {
        listProductsSelect = listProducts['listProducts'];
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getListProducts();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Column(
            children: [
              Text("Registrar producto"),
              Text(
                "Selecciona tu producto, con sus caracteristicas",
                style: TextStyle(fontSize: 15.0),
              )
            ],
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                  prefs.getColor(prefs.color),
                  prefs.getSecondaryColor(prefs.color)
                ])),
          ),
        ),
        drawer: MenuWidget(),
        body: RefreshIndicator(
            onRefresh: getRefresh,
            backgroundColor: prefs.getColor(prefs.color),
            color: Colors.white,
            child: ListView.builder(
              itemCount:
                  listProductsSelect == null ? 0 : listProductsSelect.length,
              itemBuilder: (BuildContext context, int index) {
                return Slidable(
                  key: ValueKey(index),
                  actionPane: SlidableDrawerActionPane(),
                  child: ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: RawMaterialButton(
                      onPressed: () {
                        _showModalSheet(
                            "${http.ip}/photo_product/${listProductsSelect[index]["product_image"]}");
                      },
                      elevation: 2.0,
                      fillColor: Colors.deepPurple,
                      child: Image(
                        image: NetworkImage(
                            "${http.ip}/photo_product/${listProductsSelect[index]["product_image"]}"),
                      ),
                      shape: CircleBorder(),
                    ),
                    title: Row(
                      children: [
                        CircleAvatar(
                          radius: 9.0,
                          child: Text(
                            "${listProductsSelect[index]["cod_product"]}",
                            style: TextStyle(fontSize: 9.0),
                          ),
                          backgroundColor: prefs.getColor(prefs.color),
                        ),
                        Text(
                            " Modelo: ${listProductsSelect[index]["model"]}\n Marca: ${listProductsSelect[index]["brand"]}",
                            style: TextStyle(fontSize: 13.0))
                      ],
                    ),
                    subtitle: Text(
                        "Descripción: ${listProductsSelect[index]["description_product"]}\nCategoría: ${listProductsSelect[index]["description_category"]}",
                        style: TextStyle(fontSize: 10.0)),
                    trailing: Icon(Icons.chevron_right, color: Colors.blue),
                  ),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: "Select",
                      color: Colors.green,
                      icon: Icons.save,
                      onTap: () {
                        registerProductOwner(
                            context,
                            listProductsSelect[index]["cod_product"],
                            int.parse(bloc.password));
                      },
                    )
                  ],
                );
              },
            )));
  }

  void _showModalSheet(String urlImage) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Container(
            child: Image(
              image: NetworkImage(urlImage),
            ),
            padding: EdgeInsets.all(40.0),
          );
        });
  }

  Widget refreshBG() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.green,
      child: Icon(Icons.assignment_turned_in, color: Colors.white),
    );
  }

  showSnackBar(BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Error not registered product'),
        backgroundColor: Colors.red));
  }

  registerProductOwner(
      BuildContext context, int codProduct, int ciOwner) async {
    var data = {"cod_product": codProduct, "ci_owner": ciOwner};
    Map responseRegister = await http.post(
        "/owner_management/product_manage/owner/register_product", data);
    if (responseRegister['result'] > 0) {
      print("registred: ${responseRegister["result"]}");
      int codProductOwner = responseRegister['result'];
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ProductOwnerOptions(codProductOwner)));
    } else {
      showSnackBar(context);
    }
  }
}
