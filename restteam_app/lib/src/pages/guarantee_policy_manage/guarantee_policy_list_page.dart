import 'package:flutter/material.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/guarantee_policy_manage/guarantee_policy_page.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/preferences/http.dart';

class GuaranteePolicytList extends StatefulWidget {
  static final String routeName = 'guarantee_policy_list';
  @override
  _GuaranteePolicytListState createState() => _GuaranteePolicytListState();
}

class _GuaranteePolicytListState extends State<GuaranteePolicytList> {
  final prefs = new Preferences();
  Http http = new Http();
  List guaranteePolicyList = [];
  bool state = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = Provider.of(context);
    int ci = int.parse(bloc.password);
    print(guaranteePolicyList);
    if (guaranteePolicyList.length == 0 && !state) {
      _getGuaranteePolicytList(ci);
      state = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('All Guarantee Policy'),
          backgroundColor: _getColor(prefs.color),
        ),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  _getList(BuildContext context) {
    if (guaranteePolicyList.length == 0 && state) {
      return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                padding: EdgeInsets.all(20.0),
                tooltip: "Register your Policy's Product Now!",
                //splashColor: Colors.blueAccent,
                onPressed: () {
                  Navigator.pushNamed(context, GuaranteePolicyPage.routeName);
                },
                icon: Icon(
                  Icons.add_circle,
                  color: Colors.green,
                  size: 40,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "No Policys yet!.",
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: Colors.deepPurple,
                      fontStyle: FontStyle.italic),
                ),
              )
            ],
          ));
    }
    return ListView.builder(
      itemCount: guaranteePolicyList == null ? 0 : guaranteePolicyList.length,
      itemBuilder: (context, index) =>
          _createItem(guaranteePolicyList[index], context),
    );
  }

  _createItem(var list, BuildContext context) {
    return Card(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListTile(
            leading: _getIcon(list['state_guarantee']),
            title: Text(
              "Policy: ${list['cod_policy']}",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 16,
                color: Colors.deepPurple,
              ),
            ),
            subtitle: Text(
              "Date Purchase: ${list['date_purchase']}\nDeadline: ${list['deadline']}\nCompany: ${list['name_company']}",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 15,
                color: Colors.grey,
                fontStyle: FontStyle.italic,
              ),
            ),
            onTap: () => _moreInformation(context, list),
          ),
        ));
  }

  Widget _getIcon(int state) {
    if (state == 1) {
      return Icon(Icons.check_circle, color: Colors.green);
    }
    if (state == 2) {
      return Icon(
        Icons.watch_later,
        color: Colors.orangeAccent,
      );
    }
    return Icon(
      Icons.cancel,
      color: Colors.red,
    );
  }

  _moreInformation(BuildContext context, var list) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: ListTile(
            leading: _getIcon(list['state_guarantee']),
            title: Text(
              "Cod Policy ${list['cod_policy']}",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Colors.deepPurple,
                  fontStyle: FontStyle.italic),
            ),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Nit", list['nit_client_company']),
              _getRowData("Company", list['name_company']),
              _getRowData("Phone", list['phone']),
              _getRowData("Email", list['email']),
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Invoice N°", list['nro_invoice']),
              _getRowData("Code", list['code_owner']),
              Row(
                children: <Widget>[
                  Text(
                    "Product: ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.green,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(
                "${list['description_product']}",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic),
              ),
              _getRowData("Brand", list['brand']),
              _getRowData("Model", list['model']),
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Date Purchase", list['date_purchase']),
              _getRowData("Deadline", list['deadline']),
              Divider(
                color: Colors.deepPurple,
              ),
              _getState(list['state_guarantee']),
            ],
          ),
          scrollable: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        );
      },
    );
  }

  _getRowData(var name, var value) {
    return Row(
      children: <Widget>[
        Text(
          "$name: ",
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic),
        ),
        Text(
          "$value",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              color: Colors.grey,
              fontStyle: FontStyle.italic),
        ),
      ],
    );
  }

  _getState(int state) {
    if (state == 0) {
      return Text(
        "  Expired  ",
        style: TextStyle(
            color: Colors.white,
            backgroundColor: Colors.redAccent,
            fontWeight: FontWeight.w800,
            fontSize: 15,
            fontStyle: FontStyle.italic),
      );
    }
    if (state == 1) {
      return Text("  Valid  ",
          style: TextStyle(
              color: Colors.white,
              backgroundColor: Colors.green,
              fontWeight: FontWeight.w800,
              fontSize: 15,
              fontStyle: FontStyle.italic));
    }

    return Text("  Sent  ",
        style: TextStyle(
            color: Colors.white,
            backgroundColor: Colors.orangeAccent,
            fontWeight: FontWeight.w800,
            fontSize: 15,
            fontStyle: FontStyle.italic));
  }

  _getGuaranteePolicytList(int ci) async {
    Map data = await http.get("/owner_management/guarantee_policy_list/$ci");
    var val = data['policyList'];
    print(val);
    setState(() {
      guaranteePolicyList = val;
    });
    print(val);
  }

  _getColor(color) {
    switch (color) {
      case 1:
        return Colors.deepPurple;
        break;
      case 2:
        return Colors.red;
        break;
      case 3:
        return Colors.blue;
        break;
      case 4:
        return Colors.green;
        break;
      default:
        return Colors.teal;
    }
  }
}
