import 'dart:io';
import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/home_page.dart';
import 'package:restteam_app/src/preferences/http.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';
import 'package:toast/toast.dart';

class GuaranteePolicyPage extends StatefulWidget {
  static final String routeName = 'guarantee_policy';

  final int codProductOwner;

  GuaranteePolicyPage(this.codProductOwner);

  @override
  _GuaranteePolicyPageState createState() => _GuaranteePolicyPageState();
}

class _GuaranteePolicyPageState extends State<GuaranteePolicyPage> {
  final _formKey = GlobalKey<FormState>();
  final prefs = new Preferences();
  List<DropdownMenuItem<String>> _dropMenuCompanies = [];
  String _companySelected;
  Http http = new Http();
  String _codPolicyGuarantee = "";
  DateTime _endDatePolicy = new DateTime.now();
  DateTime _dateBeginPolicy = new DateTime.now();
  String _proofNo = "";
  int _codProductOwner;
  List _companyList = [];
  List _dataRegister = [];
  File _image;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    _codProductOwner = this.widget.codProductOwner;
    getDataLoadedCompaniesAndData(_codProductOwner);
  }

  Future _onImageButtonPressed(ImageSource source,
      {BuildContext context}) async {
    try {
      print("_onImageButtonPressed");
      var pickedFile =
          await ImagePicker.pickImage(source: source, imageQuality: 70);
      print("object: " + pickedFile.path);
      setState(() {
        _image = pickedFile;
      });
      print("Done");
    } catch (e) {
      print("Error in pickImage rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr" + e);
    }
  }

  List<DropdownMenuItem> buildDropDownMenuCompanies(listCompanies) {
    List<DropdownMenuItem<String>> list = List();
    for (int i = 0; i < listCompanies.length; i++) {
      String name = listCompanies[i]["name_company"];
      list.add(
          DropdownMenuItem(value: listCompanies[i]["nit"], child: Text(name)));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Register Guarantee Policy"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                  prefs.getColor(prefs.color),
                  prefs.getSecondaryColor(prefs.color)
                ])),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.image),
              onPressed: () async {
                print(
                    "obteniendo imagen ............................................");
                await _onImageButtonPressed(ImageSource.gallery,
                    context: context);
              },
              tooltip: "Get a image of the policy",
            ),
            IconButton(
              icon: Icon(Icons.camera_alt),
              onPressed: () async {
                await _onImageButtonPressed(ImageSource.camera,
                    context: context);
              },
              tooltip: "Screen photo to the policy",
            )
          ],
        ),
        drawer: MenuWidget(),
        body: Container(
            child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  _descriptionRegister(bloc.password),
                  _showImage(context),
                  _createCodePolicy(),
                  _createBeginDate(context),
                  _createEndDate(context),
                  _createProofNo(),
                  _getDropDownLisCompanies(),
                  _createRegister(context)
                ],
              ),
            ),
          ),
        )));
  }

  getDataLoadedCompaniesAndData(int codProductOwner) async {
    try {
      Map responseData = await http.get(
          "/owner_management/guarantee_policy_manage/owner/$codProductOwner");
      _dropMenuCompanies =
          await buildDropDownMenuCompanies(responseData['companyList']);
      _companySelected = _dropMenuCompanies[0].value;
      setState(() {
        _companyList = responseData['companyList'];
        _dataRegister = responseData['dataCompany'];
      });
    } catch (e) {
      print(e);
    }
  }

  Widget _createCodePolicy() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        textCapitalization: TextCapitalization.characters,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            counter: Text("letters: ${_codPolicyGuarantee.length}"),
            labelText: "Código de Póliza",
            helperText: "Code Here",
            icon: Icon(Icons.payment)),
        validator: (value) {
          if (value.length > 1) {
            return null;
          } else {
            return "value undefined";
          }
        },
        onChanged: (value) {
          setState(() {
            _codPolicyGuarantee = value;
          });
        },
      ),
    );
  }

  Widget _createBeginDate(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          RaisedButton(
            child: Row(
              children: <Widget>[
                Icon(Icons.date_range),
                Text(" Fecha de Compra:")
              ],
            ),
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () {
              showDatePicker(
                      context: context,
                      initialDate: _dateBeginPolicy,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100))
                  .then((value) {
                setState(() {
                  _dateBeginPolicy = value;
                });
              });
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text(
              _dateBeginPolicy != null
                  ? readDate(_dateBeginPolicy.toString())
                  : "No date exists",
              style: TextStyle(fontWeight: FontWeight.w900),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _createEndDate(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          RaisedButton(
            child: Row(
              children: <Widget>[
                Icon(Icons.date_range),
                Text(" Fecha de Conclusion:")
              ],
            ),
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () {
              showDatePicker(
                      context: context,
                      initialDate: _endDatePolicy,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100))
                  .then((value) {
                setState(() {
                  _endDatePolicy = value;
                });
              });
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text(
                _endDatePolicy != null
                    ? readDate(_endDatePolicy.toString())
                    : "No date exists",
                style: TextStyle(fontWeight: FontWeight.w900),
                textAlign: TextAlign.center),
          )
        ],
      ),
    );
  }

  String readDate(String dateCompleted) {
    return dateCompleted.substring(0, dateCompleted.indexOf(' '));
  }

  Widget _createProofNo() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text("letters: ${_proofNo.length}"),
          labelText: "Nro. Factura",
          helperText: "invoice here",
          icon: Icon(Icons.payment)),
      validator: (value) {
        if (value.length < 1) {
          return "value undefined";
        } else {
          return null;
        }
      },
      onChanged: (newValue) {
        setState(() {
          _proofNo = newValue;
        });
      },
    );
  }

  Widget _createRegister(BuildContext context) {
    return RaisedButton.icon(
      icon: Icon(Icons.save),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text("Registrar Póliza de Garantía"),
      onPressed: () async {
        if (await _submit(context)) {
          Future.delayed(Duration(seconds: 2), () {
            _showToast("Guarantee Policy saved successfully.", context,
                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          });
          Navigator.pushNamed(context, HomePage.routeName);
        } else {
          _showToast("Problems to register policy, again please.", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        }
      },
    );
  }

  void _showToast(String msg, BuildContext context,
      {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  Future<bool> _submit(BuildContext context) async {
    if (_formKey.currentState.validate() && _image != null) {
      print(
          "codPolicy: $_codPolicyGuarantee, datePurchase: ${_dateBeginPolicy.toString()}\nendDate: ${_endDatePolicy.toString()}, proof: ${_proofNo}, nit:$_companySelected");
      String fileName = _image.path.split('/').last;
      FormData formData = new FormData.fromMap({
        "policy_code": _codPolicyGuarantee,
        "date_purchase": _dateBeginPolicy.toString(),
        "end_date": _endDatePolicy.toString(),
        "proof_no": _proofNo,
        "nit_company": _companySelected,
        "cod_product_owner": _codProductOwner,
        "image": await MultipartFile.fromFile(_image.path),
        "file_name": fileName
      });
      print("formdata for post: ${formData.toString()}");
      var response = await http.postImage(
          '/owner_management/guarantee_policy_manage/register', formData);
      return response.data['result'];
    } else {
      return false;
    }
  }

  Widget _getDropDownLisCompanies() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Icon(Icons.store_mall_directory),
      Text('Seleccionar Compañia: '),
      DropdownButton(
        value: _companySelected,
        items: _dropMenuCompanies,
        onChanged: (value) {
          setState(() {
            _companySelected = value;
          });
        },
      )
    ]);
  }

  Widget _descriptionRegister(CIOwner) {
    String ownerName = "";
    String description = "";
    String model = "";
    String brand = "";
    String categoryDescription = "";
    if (_dataRegister.length > 0) {
      ownerName = _dataRegister[0]["name"];
      description = _dataRegister[0]["description_product"];
      model = _dataRegister[0]["model"];
      brand = _dataRegister[0]["brand"];
      categoryDescription = _dataRegister[0]["description_category"];
    }
    return Container(
      decoration: BoxDecoration(
          color: prefs.getColor(prefs.color),
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: prefs.getSecondaryColor(prefs.color),
                blurRadius: 6.0,
                offset: Offset(0.0, 5.0))
          ]),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
            "Código de Registro: ${_codProductOwner}\nNombre de Cliente: ${ownerName} (CI: ${CIOwner})\nProducto: ${description}\nModelo: ${model}   Marca:${brand}\nCategoría: ${categoryDescription}",
            style: TextStyle(
              color: Colors.white,
            )),
      ),
    );
  }

  /*Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _image = response.file;
      });
    } else {
      print("response.file == null Alert");
    }
  }*/

  Widget _showImage(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10.0),
        child: Container(
          decoration: BoxDecoration(
              color: prefs.getColor(Colors.black45),
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: <BoxShadow>[
                BoxShadow(blurRadius: 6.0, offset: Offset(0.0, 5.0))
              ]),
          child: _image == null
              ? Image(image: AssetImage('assets/no-image.png'))
              : Image.file(_image),
        ));
  }
  /*return Padding(
        padding: const EdgeInsets.all(10.0),
        child: FutureBuilder<void>(
          future: retrieveLostData(),
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return const Text(
                  'You have not yet picked an image.',
                  textAlign: TextAlign.center,
                );
              case ConnectionState.done:
                return Container(
                  decoration: BoxDecoration(
                      color: prefs.getColor(Colors.black45),
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: <BoxShadow>[
                        BoxShadow(blurRadius: 6.0, offset: Offset(0.0, 5.0))
                      ]),
                  child: _image == null
                      ? Image(image: AssetImage('assets/no-image.png'))
                      : Image.file(File(_image.path)),
                );
              default:
                if (snapshot.hasError) {
                  return Text(
                    'Pick image/video error: ${snapshot.error}}',
                    textAlign: TextAlign.center,
                  );
                } else {
                  return const Text(
                    'You have not yet picked an image.',
                    textAlign: TextAlign.center,
                  );
                }
            }
          },
        ));
  }*/
}
