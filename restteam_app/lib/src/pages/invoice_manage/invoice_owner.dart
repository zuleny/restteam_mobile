import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/preferences/http.dart';
import 'package:restteam_app/src/preferences/share_preferences.dart';
import 'package:restteam_app/src/widgets/menu_widget.dart';

class InvoiceList extends StatefulWidget {
  const InvoiceList({Key key}) : super(key: key);
  static final String routeName = 'invoice_list';
  @override
  _InvoiceListState createState() => _InvoiceListState();
}

class _InvoiceListState extends State<InvoiceList> {
  final prefs = new Preferences();
  Http http = new Http();
  List invoiceList = [];
  bool state = false;

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    int ci = int.parse(bloc.password);
    print(invoiceList);
    if (invoiceList.length == 0 && !state) {
      _getInvoiceOwnerList(ci);
      state = true;
    }
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text("Lista de Facturas"),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                    prefs.getColor(prefs.color),
                    prefs.getSecondaryColor(prefs.color)
                  ])),
            )),
        drawer: MenuWidget(),
        body: Center(
          child: _getList(context),
        ));
  }

  void _getInvoiceOwnerList(int ci) async {
    Map data =
        await http.get("/support_service_management/service_invoice_owner/$ci");
    var val = data['list_invoice'];
    print(val);
    setState(() {
      invoiceList = val;
    });
    print(val);
  }

  _getList(BuildContext context) {
    if (invoiceList.length == 0 && state) {
      return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                padding: EdgeInsets.all(20.0),
                tooltip:
                    "Lo siento, no tienes facturas registradas en RestTeam",
                onPressed: () {
                  print("Ya te dije que no tienes facturas");
                },
                icon: Icon(
                  Icons.add_circle,
                  color: Colors.green,
                  size: 40,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "You haven't Invoice",
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: Colors.deepPurple,
                      fontStyle: FontStyle.italic),
                ),
              )
            ],
          ));
    }
    return ListView.builder(
      itemCount: invoiceList == null ? 0 : invoiceList.length,
      itemBuilder: (context, index) =>
          _createItem(invoiceList[index], context, index),
    );
  }

  _createItem(invoiceList, BuildContext context, int index) {
    return Card(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListTile(
              leading:
                  Icon(Icons.art_track, color: prefs.getColor(prefs.color)),
              title: Text(
                "Factura #${invoiceList['bill_no']}",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 16,
                  color: Colors.deepPurple,
                ),
              ),
              subtitle: Text(
                  "NIT: ${invoiceList['nit']}\nRazón Social: ${invoiceList['corporate']}",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic,
                  )),
              onTap: () => _moreInformation(context, invoiceList)),
        ));
  }

  _moreInformation(BuildContext context, invoiceList) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: ListTile(
            leading: Icon(Icons.check_circle, color: Colors.green),
            title: Text(
              "Factura #${invoiceList['bill_no']}",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: prefs.getColor(prefs.color),
                  fontStyle: FontStyle.italic),
            ),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Razón Social", invoiceList['corporate']),
              _getRowData("NIT", invoiceList['nit']),
              Divider(
                color: Colors.deepPurple,
              ),
              Row(
                children: <Widget>[
                  Text(
                    "Fecha Realización: ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.green,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(
                "${invoiceList['service_date']}",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Colors.grey,
                    fontStyle: FontStyle.italic),
              ),
              _getRowData("Solicitud #", invoiceList['request_no']),
              _getRowData(
                  "Estado del Servicio",
                  invoiceList['state_support_service']
                      ? "Terminado"
                      : "En Proceso"),
              Divider(
                color: Colors.deepPurple,
              ),
              _getRowData("Costo Total: ", invoiceList['total_cost'] + " Bs."),
              _getRowData("Persona Responsable: ", invoiceList['username']),
            ],
          ),
          scrollable: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        );
      },
    );
  }

  _getRowData(var name, var value) {
    return Row(
      children: <Widget>[
        Text(
          "$name: ",
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic),
        ),
        Text(
          "$value",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              color: Colors.grey,
              fontStyle: FontStyle.italic),
        ),
      ],
    );
  }
}
