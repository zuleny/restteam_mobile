import 'package:flutter/material.dart';

import 'package:restteam_app/src/bloc/provider.dart';
import 'package:restteam_app/src/pages/guarantee_policy_manage/guarantee_policy_list_page.dart';
import 'package:restteam_app/src/pages/home_page.dart';
import 'package:restteam_app/src/pages/invoice_manage/invoice_owner.dart';
import 'package:restteam_app/src/pages/login/login_page.dart';
import 'package:restteam_app/src/pages/owner_manage/owner_activity_list_page.dart';
import 'package:restteam_app/src/pages/owner_manage/owner_notifications_list_page.dart';
import 'package:restteam_app/src/pages/owner_manage/owner_page.dart';
import 'package:restteam_app/src/pages/product_manage/product_list_page.dart';
import 'package:restteam_app/src/pages/product_manage/product_register_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_list_page.dart';
import 'package:restteam_app/src/pages/request_support_service/request_support_page.dart';
import 'package:restteam_app/src/pages/setting_page.dart';
import 'package:restteam_app/src/pages/wait_page.dart';

import 'preferences/share_preferences.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prefs = new Preferences();
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'RestTeam',
        initialRoute: LoginPage.routeName,
        routes: {
          HomePage.routeName: (BuildContext context) => HomePage(),
          SettingPage.routeName: (BuildContext context) => SettingPage(),
          RequestSupport.routeName: (BuildContext context) => RequestSupport(),
          Wait.routeName: (BuildContext context) => Wait(),
          LoginPage.routeName: (BuildContext context) => LoginPage(),
          RequestSupportList.routeName: (BuildContext context) =>
              RequestSupportList(),
          ProductPage.routeName: (BuildContext context) => ProductPage(),
          GuaranteePolicytList.routeName: (BuildContext context) =>
              GuaranteePolicytList(),
          Owner.routeName: (BuildContext context) => Owner(),
          OwnerActivity.routeName: (BuildContext context) => OwnerActivity(),
          ProductList.routeName: (BuildContext context) => ProductList(),
          InvoiceList.routeName: (BuildContext context) => InvoiceList(),
          OwnerNotification.routeName: (BuildContext context) => OwnerNotification()
        },
        theme: ThemeData(primaryColor: prefs.getColor(prefs.color)),
      ),
    );
  }
}
